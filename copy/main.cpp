//
// Created by knork on 1/12/18.
//


#include <CL/cl.hpp>
#include <fstream>
#include <iostream>
#include <chrono>
#include <numeric>
#include "head.h"

int main() {


	u_int32_t size = 1024*1024 /4;
	float *A = new float[size];
	for (int i = 0; i < size; i++) {
		A[i] = rand() / (float) RAND_MAX;
	}

	using namespace cl;
	using namespace std;

	Platform platform = Platform::getDefault();
	Device device = Device::getDefault();
	Context context({device});
	CommandQueue queue(context, device);

	Buffer b_A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float)*size,A);
	Buffer b_B(context, CL_MEM_WRITE_ONLY, sizeof(float) * size);
	queue.enqueueMigrateMemObjects({b_A, b_B}, 0); // Non-blocking



	float *B = new float[size];
	queue.enqueueReadBuffer(b_B, CL_TRUE, 0, sizeof(float) * size, B);
	queue.finish();




	double norm = 0.0f;
	for (int i = 0; i < size; i++) {
		norm += A[i] - B[i];
	}
	cout << "norm:" << norm << endl;
	// Build error Check!
	// Include Header !


	return 0;
}

int backup(){

	u_int32_t size = 1024*1024 /4;
	float *A = new float[size];
	for (int i = 0; i < size; i++) {
		A[i] = rand() / (float) RAND_MAX;
	}

	using namespace cl;
	using namespace std;

	Platform platform = Platform::getDefault();
	Device device = Device::getDefault();
	Context context({device});
	CommandQueue queue(context, device);

	Buffer b_A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float)*size,A);
	Buffer b_B(context, CL_MEM_WRITE_ONLY, sizeof(float) * size);
	queue.enqueueMigrateMemObjects({b_A, b_B}, 0); // Non-blocking


	ifstream src_header("head.h");
	string head((istreambuf_iterator<char>(src_header)), istreambuf_iterator<char>());
	string path("kernels/copy.cl");
	ifstream src_str(path);
	string src((istreambuf_iterator<char>(src_str)), istreambuf_iterator<char>());
	src= head+src;
	Program program(context, src);
	if (program.build({device}) != CL_SUCCESS) {
		cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << endl;
		exit(1);
	}
	queue.finish(); // copy done

	const std::vector<unsigned long> binSizes = program.getInfo<CL_PROGRAM_BINARY_SIZES>();
	std::vector<char> binData(std::accumulate(binSizes.begin(), binSizes.end(), 0));
	char *binChunk = &binData[0];


//A list of pointers to the binary data
	std::vector<char *> binaries;
	for (unsigned int i = 0; i < binSizes.size(); ++i) {
		binaries.push_back(binChunk);
		binChunk += binSizes[i];
	}

	auto lastindex = path.find_last_of(".");
	std::string binary_path = path.substr(0, lastindex);
	binary_path += ".ptx";

	program.getInfo(CL_PROGRAM_BINARIES, &binaries[0]);
	std::ofstream binaryfile(binary_path, std::ios::binary);
	for (unsigned int i = 0; i < binaries.size(); ++i)
		binaryfile.write(binaries[i], binSizes[i]);

	Kernel copy(program, "copy");
	copy.setArg(0, b_A);
	copy.setArg(1, b_B);

	queue.enqueueNDRangeKernel(copy, 0, NDRange(size), NDRange(32), 0, 0);
	queue.finish();

	float *B = new float[size];
	queue.enqueueReadBuffer(b_B, CL_TRUE, 0, sizeof(float) * size, B);
	queue.finish();


	double norm = 0.0f;
	for (int i = 0; i < size; i++) {
		norm += A[i] - B[i];
	}
	cout << "norm:" << norm << endl;

}