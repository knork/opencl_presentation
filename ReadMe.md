# Install and Dependencies (Linux):

1. Make your system ready to compile C++ code:
```
apt install build-essentials cmake 
git clone --recursive -j8
```
2. Install the proper [graphics driver](https://wiki.debian.org/GraphicsCard) for your system
3. Install the proper opencl driver for your system:
```
apt install opencl-headers # all
apt install beignet-opencl-icd beignet # intel
apt install nvidia-opencl-dev # nvidia
apt install amd-libopencl1 # amd
```
4. Build and run
```
mkdir build
cd build
cmake ..
make -j8
./main 
```

# Windows

You will need to install a C++ 11 compiler, Cmake as well as openCL header as well as libraries 
and make them available to cmake. It should work but it has not been tested though.

# Further information
The idea of these implementations is based on:

https://cnugteren.github.io/tutorial/pages/page1.html



