//
// Created by knork on 1/10/18.
//

#ifndef CLMATRIXMULT_HOST_MULT_H
#define CLMATRIXMULT_HOST_MULT_H

#include <omp.h>

//		   __k__		  __n__		   __n__
//		  |			     |			  |
//		 m|  A     *    K|  B	  =  m|
//		  |			     |			  |
void hostMatrixMult(float* A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N, double& exec_time){


	int m=0;
	int n = 0;
	clock_t begin = clock();
	#pragma parrallel for collapse(2)
	for ( m=0; m<M; m++) {
		for ( n=0; n<N; n++) { // go through C
			float acc = 0.0f;
			for (int k=0; k<K; k++) {
				acc += A[k*M + m] * B[n*K + k];
			}

			C[n*M + m] = acc;
		}
	}
	exec_time = double(clock() - begin) / CLOCKS_PER_SEC;
}


#endif //CLMATRIXMULT_HOST_MULT_H
