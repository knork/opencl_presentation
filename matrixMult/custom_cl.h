//
// Created by knork on 1/10/18.
//

#ifndef CLMATRIXMULT_CLNAIV_H
#define CLMATRIXMULT_CLNAIV_H

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <numeric>
#include <functional>
#include <chrono>
#include  "settings.h"

using namespace std::chrono;
typedef high_resolution_clock::time_point h_time;
typedef high_resolution_clock h_clock;



//		   __k__		  __n__		   __n__
//		  |			     |			  |
//		 m|  A     *    k|  B	  =  m|
//		  |			     |			  |
void defaultMultSetup(float *A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N,double& exec_time,
					  std::string path, std::string kernelName,
						std::function<void ( cl::CommandQueue&, cl::Kernel& kern)> exec){
	using namespace cl;
	using namespace std;

	int err;
	Platform platform = Platform::getDefault();
	Device device = Device::getDefault();

	Context context({device});
	CommandQueue queue(context, device);

	Buffer b_A(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(float) * M * K, A);
	Buffer b_B(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(float) * K * N, B);
	Buffer b_C(context, CL_MEM_READ_WRITE  , sizeof(float) * M * N, nullptr);
	queue.enqueueMigrateMemObjects({b_A, b_B,b_C}, 0);
	queue.finish();

	ifstream t(path);
	string source((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
	t = ifstream("settings.h");
	string header((istreambuf_iterator<char>(t)),istreambuf_iterator<char>());
	string included = header + source;

	Program program(context, included);
	if (program.build({device}) != CL_SUCCESS) {
		std::cout << "Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
		exit(1);
	}


	const std::vector<unsigned long> binSizes = program.getInfo<CL_PROGRAM_BINARY_SIZES>();
	std::vector<char> binData (std::accumulate(binSizes.begin(),binSizes.end(),0));
	char* binChunk = &binData[0] ;


	// Nvidia outputs "binaries" in assembly format
	std::vector<char*> binaries;
	for(unsigned int i = 0; i<binSizes.size(); ++i) {
		binaries.push_back(binChunk) ;
		binChunk += binSizes[i] ;
	}
	auto lastindex = path.find_last_of(".");
	std::string binary_path = path.substr(0, lastindex);
	binary_path+=".ptx";

	program.getInfo(CL_PROGRAM_BINARIES , &binaries[0] ) ;
	std::ofstream binaryfile(binary_path, std::ios::binary);
	for (unsigned int i = 0; i < binaries.size(); ++i)
		binaryfile.write(binaries[i], binSizes[i]);

	
	Kernel mult(program, kernelName.c_str());
	mult.setArg(0,b_A);
	mult.setArg(1,b_B);
	mult.setArg(2,b_C);
	mult.setArg(3,M);
	mult.setArg(4,N);
	mult.setArg(5,K);

	h_time begin = h_clock::now();
	exec(queue,mult);
	queue.finish();
	exec_time = (duration_cast<duration<double>>(h_clock::now()-begin)).count();
	queue.enqueueReadBuffer(b_C,CL_TRUE,0,sizeof(float) * M * N,C);
	queue.finish();

}

void clNaivMult(float *A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N, double& exec_time) {

	std::function<void (
			cl::CommandQueue&, cl::Kernel& kern)> exec =
			[&](cl::CommandQueue &queue, cl::Kernel& kern){
		using namespace cl;
		NDRange local(32,32);
		NDRange global(M,N);
		queue.enqueueNDRangeKernel(kern,NDRange(0,0),global,local,0,0);
		queue.finish();
	};
	defaultMultSetup(A, B, C, K, M, N,exec_time, "kernels/naiv.cl", "naiv",exec);
}




void clTilingMult(float *A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N, double &exec_time) {

	std::function<void (
			cl::CommandQueue&, cl::Kernel& kern)> exec =
			[&](cl::CommandQueue &queue, cl::Kernel& kern){
		using namespace cl;
		NDRange local(TS,TS);
		NDRange global(M,N);
		queue.enqueueNDRangeKernel(kern,NDRange(0,0),global,local,0,0);
		queue.finish();
	};
	defaultMultSetup(A, B, C, K, M, N,exec_time, "kernels/tiling.cl", "tiling",exec);
}

void clWPT(float *A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N, double &exec_time){
	using namespace cl;

	std::function<void (
			cl::CommandQueue&, cl::Kernel& kern)> exec =
			[&](cl::CommandQueue &queue, cl::Kernel& kern){
				using namespace cl;
				const int work_size = TS;
				NDRange local(work_size,work_size/WPT);
				NDRange global(M,N/WPT);
				queue.enqueueNDRangeKernel(kern,NDRange(0,0),global,local,0,0);
				queue.finish();
			};
	defaultMultSetup(A, B, C, K, M, N,exec_time, "kernels/WPT.cl", "wpt",exec);

}



#endif //CLMATRIXMULT_CLNAIV_H
