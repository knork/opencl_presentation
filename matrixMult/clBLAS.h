//
// Created by knork on 1/10/18.
//

#ifndef CLMATRIXMULT_CLBLAS_H
#define CLMATRIXMULT_CLBLAS_H

#include <CL/cl.hpp>
#include <clblast.h>
#include <chrono>

using namespace std::chrono;
typedef high_resolution_clock::time_point h_time;
typedef high_resolution_clock h_clock;

void blasMatrixMult(float *A, float *B, float *C, u_int32_t K, u_int32_t M, u_int32_t N, double & exec_time) {
	using namespace cl;
	Platform platform = Platform::getDefault();
	Device device = Device::getDefault();
	Context context({device});
	CommandQueue queue(context, device);
	Buffer b_A(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float) * M * K, A);
	Buffer b_B(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, sizeof(float) * K * N, B);
	Buffer b_C(context, CL_MEM_READ_ONLY| CL_MEM_USE_HOST_PTR, sizeof(float) * M * N, C);
	queue.enqueueMigrateMemObjects({b_A, b_B,b_C}, 0);
	queue.finish();

	std::string name=	device.getInfo<CL_DEVICE_NAME>();
	std::cout << name << std::endl;
// use CLBlas to generate refrence
	{
		cl_event event;
		cl_command_queue q = (queue());
		const float alpha = 1.0f;
		const float beta = 0.0f;
		const auto a_ld = K;
		const auto b_ld = N;
		const auto c_ld = N;
		h_time begin = h_clock::now();
		auto status = clblast::Gemm(clblast::Layout::kColMajor,
									clblast::Transpose::kNo, clblast::Transpose::kNo,
									M, N, K,
									alpha,
									b_A(), 0, a_ld,
									b_B(), 0, b_ld,
									beta,
									b_C(), 0, c_ld,
									&q, &event);

		if (status == clblast::StatusCode::kSuccess) {
			clWaitForEvents(1, &event);
			clReleaseEvent(event);
		}
		exec_time = (duration_cast<duration<double>>(h_clock::now()-begin)).count();
	}
	queue.enqueueReadBuffer(b_C,CL_TRUE,0,sizeof(float) * M * N,C);
	queue.finish();

}

#endif //CLMATRIXMULT_CLBLAS_H
