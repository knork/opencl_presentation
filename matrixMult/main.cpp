#include <sys/types.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <CL/cl.hpp>

#include "host_mult.h"
#include "custom_cl.h"
#include "clBLAS.h"
#include <string>
#include <chrono>

using namespace std::chrono;
typedef high_resolution_clock::time_point h_time;
typedef high_resolution_clock h_clock;

float norm(float* A, float* B, u_int32_t N, u_int32_t M){
	double norm =0.0f;
	for(int n=0;n<N;n++){
		for(int m=0;m<M;m++){
			norm += A[n*M+m]-B[n*M+m];
		}
	}
	return norm;
}


double refTime = 0;
void execAndLog(void (*func)(float*,float*,float*, u_int32_t,u_int32_t,u_int32_t,double&),
				float* A, float *B, float *C,
				u_int32_t K,u_int32_t M, u_int32_t N,
				double & exec_time, float* refrence,const std::string& name){


			h_time begin = h_clock::now();
			func(A,B,C,K,M,N,exec_time);
			double total = (duration_cast<duration<double>>(h_clock::now()-begin)).count();

			if(C==refrence )
				refTime = exec_time;


			printf("%-13s  %.3fs (%6.2f\%) \t  %.3f \t  %.3fs \n",name.c_str(),
				   exec_time,refTime/exec_time*100,norm(C,refrence,N,M),total);
			fflush(stdout);
}



int main(){

	const u_int32_t minSize = 1024;
	const u_int32_t maxSize =4096;



	// double matrix size on each loop through
	for( int size=minSize; size <= maxSize;size*=2){

		h_time total_h = high_resolution_clock::now();

		u_int32_t K=size;
		u_int32_t M=size;
		u_int32_t N=size;

		printf("\n\n");
		printf("Matrix size of C is: %d \n", K);

		float * A = new float[M*K];
		float * B = new float[K*N];
		float * C = new float[M*N];
		float * refrence = new float[M*N];

		for(int i=0;i<M*K;i++){
			A[i]= (float) rand()/ (float)RAND_MAX;
		}

		for(int i=0;i<K*N;i++){
			B[i]= (float) rand()/ (float)RAND_MAX;
		}
		if(size ==minSize) {
			double some;
			blasMatrixMult(A, B, C, K, M, N, some);
		}

		double exec_time;
		u_int32_t memory_used = sizeof(float)*M*K + sizeof(float)*K*N + sizeof(float)*M*N;
		memory_used = memory_used /( 1024 * 1024 ); //MB


		printf("All Matrices take up %d MB\n", memory_used);
		printf("Type\t       ExecTime\t\t\t\t  Norm \t  Total Time  \n");
		printf("--------------------------------------------------------\n");
		execAndLog(hostMatrixMult,A,B,refrence,K,M,N,exec_time,refrence,"Host");
//		execAndLog(blasMatrixMult,A,B,refrence,K,M,N,exec_time,refrence,"Refrence");
//
		execAndLog(clNaivMult,A,B,C,K,M,N,exec_time,refrence,"clNaiv");
//		execAndLog(clTilingMult,A,B,C,K,M,N,exec_time,refrence,"clTiled");
//		execAndLog(clWPT,A,B,C,K,M,N,exec_time,refrence,"clWPT");

		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		duration<double> time_span = duration_cast<duration<double>>(t2 - total_h);

		delete(A);
		delete(B);
		delete(C);
		delete(refrence);

	}

	return 0;

}