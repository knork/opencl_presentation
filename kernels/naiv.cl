
__kernel void naiv(
		__global const float *A,
		__global const float *B,
		__global float *C,
		const int K,
		const int M,
		const int N
) {

	const int globalRow =get_global_id(0);
	const int globalCol = get_global_id(1);


	// Compute a single element (loop over K)
	float acc = 0.0f;
	for (int k = 0; k < K; k++) {
		// N * M * K * 2 loads (A and B)
		acc += A[k * M + globalRow] * B[globalCol * K + k];
	}

	// Store the result (N*M)
	C[globalCol * M + globalRow] = acc;
}

