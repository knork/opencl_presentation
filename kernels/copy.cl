__kernel void copy(
		__global const float *A,
		__global float *B
) {

	int id = get_global_id(0);
	B[id] = A[id];
}

